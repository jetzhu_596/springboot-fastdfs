package com.springboot.fastdfs.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @desc 测试页面
 * @author chay
 * @version 2.0.0
 * @date 2018-07-12
 */
@Controller
@RequestMapping("/fastdfs")  
public class Web {

	@RequestMapping("")
    public String index() {
		return "index";
    }
	
}
